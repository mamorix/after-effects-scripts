// Basic Hose Maker.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Basic Hose Maker
// Version: 1.1
//
// Description:
// Basic Hose Maker will make recreating a simple, or basic, RubberHose system
// from a preexisting stoke simple and easy. Just select a brand new RubberHose
// layer and a shape layer you want to mimic. Basic Hose Maker will attempt to
// use the same stroke width, stroke color, and stroke length as well as place
// the controllers appropriately.
//
// Use:
// Select a brand new RubberHose layer and a shape layer, run the script, and
// the RubberHose layer should look exactly like the shape layer.
//
// Note:
// Special thanks to Paul Conigliaro for the getPathLength() function and
// hernan Torrisi for the getCurveLength() function. Basic Hose Maker wouldn't
// work without these dudes who are way smarter than me.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    // Paul Conigliaro
    function getPathLength(shapePath) {
        var len = 0;
        var verts = shapePath.vertices;
        var numVerts = verts.length;
        var ins = shapePath.inTangents;
        var outs = shapePath.outTangents;

        for (var i = 0; i < numVerts - 1; i++) {
            len += getCurveLength(verts[i],verts[i+1],outs[i],ins[i+1]);
        }

        if (shapePath.closed == true) {
            len += getCurveLength(verts[numVerts-1],verts[0],outs[numVerts-1],ins[0]);
        }

        return len;
    }

    // Hernan Torrisi
    function getCurveLength(initPos, endPos, outBezier, inBezier) {
        var k, curveSegments = 100, point, lastPoint = null, ptDistance, absToCoord, absTiCoord, triCoord1, triCoord2, triCoord3, liCoord1, liCoord2, ptCoord, perc, addedLength = 0, i, len;
        for (k = 0; k < curveSegments; k += 1) {
            point = [];
            perc = k / (curveSegments - 1);
            ptDistance = 0;
            absToCoord = [];
            absTiCoord = [];
            len = outBezier.length;
            for (i = 0; i < len; i += 1) {
                if (absToCoord[i] === null || absToCoord[i] === undefined) {
                    absToCoord[i] = initPos[i] + outBezier[i];
                    absTiCoord[i] = endPos[i] + inBezier[i];
                }
                triCoord1 = initPos[i] + (absToCoord[i] - initPos[i]) * perc;
                triCoord2 = absToCoord[i] + (absTiCoord[i] - absToCoord[i]) * perc;
                triCoord3 = absTiCoord[i] + (endPos[i] - absTiCoord[i]) * perc;
                liCoord1 = triCoord1 + (triCoord2 - triCoord1) * perc;
                liCoord2 = triCoord2 + (triCoord3 - triCoord2) * perc;
                ptCoord = liCoord1 + (liCoord2 - liCoord1) * perc;
                point.push(ptCoord);
                if (lastPoint !== null) {
                    ptDistance += Math.pow(point[i] - lastPoint[i], 2);
                }
            }
            ptDistance = Math.sqrt(ptDistance);
            addedLength += ptDistance;
            lastPoint = point;
        }
        return addedLength;
    }

    function isRubberHoseLayer(layer) {
        try {
            var hasStyleGroup = (layer.property("ADBE Root Vectors Group").property(1).name === "Style");
            var hasAdminGroup = (layer.property("ADBE Root Vectors Group").property(2).name === "Admin");
        } catch (err) {
            var hasStyleGroup = false;
            var hasAdminGroup = false;
        }
        return (hasStyleGroup && hasAdminGroup);
    }

    function hoseMaker(comp, hoseLayer, shapeLayer) {
        var startController = comp.layer(hoseLayer.property("ADBE Root Vectors Group").property("Admin").property("ADBE Vectors Group").property("B").property("ADBE Vectors Group").property(1).name);
        var endController = comp.layer(hoseLayer.property("ADBE Root Vectors Group").property("Admin").property("ADBE Vectors Group").property("A").property("ADBE Vectors Group").property(1).name);

        var strokePath = shapeLayer.property("ADBE Root Vectors Group").property("ADBE Vector Group").property("ADBE Vectors Group").property("ADBE Vector Shape - Group").property("ADBE Vector Shape").value;
        var strokeColor = shapeLayer.property("ADBE Root Vectors Group").property("ADBE Vector Group").property("ADBE Vectors Group").property("ADBE Vector Graphic - Stroke").property("ADBE Vector Stroke Color").value;
        var strokeWidth = shapeLayer.property("ADBE Root Vectors Group").property("ADBE Vector Group").property("ADBE Vectors Group").property("ADBE Vector Graphic - Stroke").property("ADBE Vector Stroke Width").value;

        var strokeLength = Math.round(getPathLength(strokePath) * 100) / 100;

        var effect = endController.property("ADBE Effect Parade").property("RubberHose 2");
            effect.property("Hose Length").setValue(strokeLength);
            effect.property("Realism").setValue(100);

        var vertices = strokePath.vertices;
        var firstVert = vertices[0];
        var lastVert = vertices[vertices.length - 1];

        var anchorPoint = shapeLayer.transform.position.value;

        var startPosition = (altKey === true) ? anchorPoint + lastVert : anchorPoint + firstVert;
        var endPosition = (altKey === true) ? anchorPoint + firstVert : anchorPoint + lastVert;

        startController.transform.position.setValue(startPosition);
        endController.transform.position.setValue(endPosition);

        var hoseStroke = hoseLayer.property("ADBE Root Vectors Group").property("ADBE Vector Group").property("ADBE Vectors Group").property("ADBE Vector Group").property("ADBE Vectors Group").property("ADBE Vector Graphic - Stroke");
            hoseStroke.property("ADBE Vector Stroke Color").setValue(strokeColor);
            hoseStroke.property("ADBE Vector Stroke Width").setValue(strokeWidth);

        return;
    }

    try {
        var altKey = ScriptUI.environment.keyboardState.altKey;
        app.beginUndoGroup("Create RubberHose Rig");
        var comp = app.project.activeItem;
        var layers = comp.selectedLayers;
        var numLayers = layers.length;
        if (numLayers === 2) {
            var layerOne = layers[0];
            var layerTwo = layers[1];
            if (isRubberHoseLayer(layerOne) === true) {
                hoseMaker(comp, layerOne, layerTwo);
            } else if (isRubberHoseLayer(layerTwo) === true) {
                hoseMaker(comp, layerTwo, layerOne);
            } else {
                throw "Please select a RubberHose layer and a shape layer."
            }
        } else {
            throw "Please select two layers.";
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
