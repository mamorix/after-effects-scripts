// Cel Shader.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Cel Shader
// Version: 1.1
//
// Description:
// In daily 2D animation world I often want to add a shadow or a highlight
// using the same color but in a blend mode such as multiply/screen and
// with reduced opacity. Usually this is fine but when multiple shadows overlap
// things get weird. This script will calculate what the final color will be
// and then just apply it directly.
//
// Use:
// Select a single color property and run this script. Use the popup to adjust
// your final color and then apply it.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    function calculateMultiply(a, b) {
        var r = a[0] * b[0];
        var g = a[1] * b[1];
        var b = a[2] * b[2];
        return [r, g, b, 1];
    }

    function calculateScreen(a, b) {
        var r = 1 - ((1 - a[0]) * (1 - b[0]));
        var g = 1 - ((1 - a[1]) * (1 - b[1]));
        var b = 1 - ((1 - a[2]) * (1 - b[2]));
        return [r, g, b, 1];
    }

    function calculateAlpha(a, b, o) {
        var r = ((b[0] * o) + ((a[0] * 1) * (1 - o))) / (o + (1 * (1 - o)));
        var g = ((b[1] * o) + ((a[1] * 1) * (1 - o))) / (o + (1 * (1 - o)));
        var b = ((b[2] * o) + ((a[2] * 1) * (1 - o))) / (o + (1 * (1 - o)));
        return [r, g, b, 1];
    }

    function calculateColor(colorA, blendMode, opacity) {
        if (blendMode === "Multiply") {
            var colorB = calculateMultiply(colorA, colorA);
        } else if (blendMode === "Screen") {
            var colorB = calculateScreen(colorA, colorA);
        }
        return calculateAlpha(colorA, colorB, opacity);
    }

    function buildPreview(preview, a, b) {
        while (preview.children.length > 0) {
            preview.remove(0);
        }
        var colors = [[a, a, null], [a, b, b], [null, b, b]];
        for (var r = 0; r < 3; r++) {
            var row = preview.add("group");
            row.alignChildren = ["left", "top"];
            row.orientation = "row";
            row.spacing = 0;
            for (var c = 0; c < 3; c++) {
                var group = row.add("group");
                group.preferredSize = [20, 20];
                var color = colors[r][c];
                if (color !== null) {
                    group.graphics.backgroundColor = group.graphics.newBrush(group.graphics.BrushType.SOLID_COLOR, color);
                }
            }
        }
        preview.layout.layout(true);
    }

    function getShadedColor(selectedColor) {
        var colorA = selectedColor;
        var blendMode = "Multiply";
        var opacity = 1;
        var colorC = calculateColor(colorA, blendMode, opacity);

        var win = new Window("dialog", "Cel Shader v1.1");
        win.alignChildren = ["left", "top"];
        win.orientation = "column";

        var content = win.add("group");
        content.alignChildren = ["left", "top"];
        content.orientation = "row";

        var settings = content.add("panel", undefined, "Settings");
        settings.alignChildren = ["left", "top"];
        settings.orientation = "column";

        var blendModeRow = settings.add("group");
        blendModeRow.alignChildren = ["left", "middle"];
        blendModeRow.orientation = "row";

        var blendModeLabel = blendModeRow.add("statictext", undefined, "Blend Mode:");
        blendModeLabel.preferredSize.width = 70;

        var blendModeDropdown = blendModeRow.add("dropdownlist", undefined, ["Multiply", "Screen"]);
        blendModeDropdown.preferredSize.width = 100;
        blendModeDropdown.selection = 0;
        blendModeDropdown.onChange = function() {
            blendMode = this.selection.text;
            colorC = calculateColor(colorA, blendMode, opacity);
            buildPreview(preview, colorA, colorC);
        }

        var opacityRow = settings.add("group");
        opacityRow.alignChildren = ["left", "middle"];
        opacityRow.orientation = "row";

        var opacityLabel = opacityRow.add("statictext", undefined, "Opacity:");
        opacityLabel.preferredSize.width = 70;

        var opacitySlider = opacityRow.add("slider", undefined, 100, 0, 100);
        opacitySlider.preferredSize.width = 100;
        opacitySlider.onChanging = function() {
            opacityValue.text = Math.floor(this.value) + "%";
            opacity = Math.floor(this.value) * 0.01;
            colorC = calculateColor(colorA, blendMode, opacity);
            buildPreview(preview, colorA, colorC);
        }

        var opacityValue = opacityRow.add("statictext", undefined, "100%");
        opacityValue.enabled = false;

        var preview = content.add("group");
        preview.alignChildren = ["left", "top"];
        preview.orientation = "column";
        preview.spacing = 0;
        preview.margins.top = 14;

        buildPreview(preview, colorA, colorC);

        var buttonGroup = win.add("group");
        buttonGroup.alignChildren = ["left", "top"];
        buttonGroup.orientation = "row";
        buttonGroup.add("button", undefined, "Cancel");
        buttonGroup.add("button", undefined, "Ok");

        var shadedColor = (win.show() === 1) ? colorC : null;
        return shadedColor;
    }

    try {
        app.beginUndoGroup("Cel Shade Color");
        var comp = app.project.activeItem;
        var properties = comp.selectedProperties;
        var property = properties[properties.length - 1];
        if (property.propertyType !== PropertyType.PROPERTY) {
            throw "Oops! Please select a single color property."
        }
        if (property.propertyValueType !== PropertyValueType.COLOR) {
            throw "Oops! Please select a single color property."
        }
        var selectedColor = property.value;
        var shadedColor = getShadedColor(selectedColor);
        if (shadedColor !== null) {
            property.setValue(shadedColor);
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
