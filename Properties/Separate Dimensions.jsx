// Separate Dimensions.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Separate Dimensions
// Version: 1.1
//
// Description:
// This script will separate the dimensions of the position property on all
// selected layers. This script will automatically deselect all layers. Sorry,
// personal preference, feel free to remove line 36 if you want.
//
// Use:
// Select your layer(s) and run the script to seperate the dimensions. Hold ALT
// to reconnect the dimensions.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Separate Dimensions");
        var altKey = ScriptUI.environment.keyboardState.altKey;
        var toggle = (altKey === true) ? false : true;
        var comp = app.project.activeItem;
        var layers = comp.selectedLayers;
        var numLayers = layers.length;
        for (var l = 0; l < numLayers; l++) {
            var layer = layers[l];
            layer.transform.position.dimensionsSeparated = toggle;
            layer.selected = false;
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
