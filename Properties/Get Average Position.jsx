// Get Average Position.jsx
// Copyright (c) 2016-2019 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Get Average Position
// Version: 1.0
//
// Description:
// Move the last selected layer to the average position of all other selected
// layers in composition space.
//
// Use:
// Select several layers and then the target layer. Click to position.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        var comp = app.project.activeItem;
        var layers = comp.selectedLayers;
        var numLayers = layers.length - 1;
        var xTotal = 0;
        var yTotal = 0;
        for (var l = 0; l < numLayers; l++) {
            var layer = layers[l];
            var sliderEffect = layer.property("ADBE Effect Parade").addProperty("ADBE Slider Control");
            var sliderProperty = sliderEffect.property(1);
            sliderProperty.expression = "thisLayer.toComp(thisLayer.transform.anchorPoint)[0]"; 
            var xPosition = sliderProperty.value;
            sliderProperty.expression = "thisLayer.toComp(thisLayer.transform.anchorPoint)[1]";
            var yPosition = sliderProperty.value;
            sliderEffect.remove();
            xTotal += xPosition;
            yTotal += yPosition;
        }
        var xAverage = xTotal / numLayers;
        var yAverage = yTotal / numLayers;
        var targetLayer = layers[numLayers];
        targetLayer.property("ADBE Transform Group").property("ADBE Position").setValue([xAverage, yAverage]);
    } catch (err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})()