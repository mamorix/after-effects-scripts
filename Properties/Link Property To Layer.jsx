// Link Property To Layer.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Link Property To Layer
// Version: 1.1
//
// Description:
// Link a position-like property to a layer using the toComp() method. Just
// select a layer and the property you want to link. It doesn't matter what
// order you select them in.
//
// Use:
// Select a position-like property and a layer. Run this script to link them
// together. It doesn't matter what order you select them in.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    function hasAnchorPoint(layer) {
        return (layer.transform.anchorPoint !== undefined);
    }

    function linkPropertyToLayer(layerA, layerB) {
        var layerReference = "thisComp.layer(\"" + layerB.name + "\")";
        var numProperties = layerA.selectedProperties.length;
        var property = layerA.selectedProperties[numProperties - 1];
        var argument = (hasAnchorPoint(layerB) === true) ? layerReference + ".transform.anchorPoint" : "[0,0,0]";
        property.expression = layerReference + ".toComp(" + argument + ");";
    }

    try {
        app.beginUndoGroup("Link Property To Layer");
        var comp = app.project.activeItem;
        var layers = comp.selectedLayers;
        var layerOne = layers[0];
        var layerTwo = layers[1];
        var layerOneNumSelectedProperties = layerOne.selectedProperties.length;
        var layerTwoNumSelectedProperties = layerTwo.selectedProperties.length;
        if (layerOneNumSelectedProperties > layerTwoNumSelectedProperties) {
            linkPropertyToLayer(layerOne, layerTwo);
        } else {
            linkPropertyToLayer(layerTwo, layerOne);
        }
    } catch (err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();