// Posterize Property.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Posterize Property
// Version: 1.0
//
// Description:
// Apply the posterizeTime(fps); value; to all selected properties. Sometimes
// you need to posterize just a property but not an entire layer or
// composition.
//
// Use:
// Select your properties and run this script to apply the expression. Default
// fps is 12. Hold alt for a popup option.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Posterize Property");

        var fps = 12;
        var altKey = ScriptUI.environment.keyboardState.altKey;
        if (altKey === true) {
            fps = parseInt(prompt("Enter frames per second", "12"));
        }

        var comp = app.project.activeItem;
        var properties = comp.selectedProperties;
        var numProperties = properties.length;
        for (var p = 0; p < numProperties; p++) {
            var property = properties[p];
            if (property.canSetExpression === true) {
                property.expression = "posterizeTime(" + fps + "); value;";
            }
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
