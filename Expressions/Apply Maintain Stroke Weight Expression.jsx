// Apply Maintain Stroke Weight Expression.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Apply Maintain Stroke Weight Expression
// Version: 1.1
//
// Description:
// Apply an expression to maintain the stroke weight no matter how much a layer
// is scaled or parented.
//
// Use:
// Select your properties and run this script to apply the expression.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Apply Maintain Stroke Weight Expression");
        var comp = app.project.activeItem;
        var properties = comp.selectedProperties;
        var numProperties = properties.length;
        for (var p = 0; p < numProperties; p++) {
            var property = properties[p];
            if (property.canSetExpression === true) {
                property.expression = "s = length(toComp([0,0]),toComp([0.7071,0.7071])); (s) ? value / s : 0;";
            }
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
