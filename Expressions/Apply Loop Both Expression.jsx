// Apply Loop Both Expression.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Apply Loop Both Expression
// Version: 1.2
//
// Description:
// Apply the loopIn() + loopOut() - value; expression to loop both before and
// after keyframes on the selected properties. Use modifier keys to set the
// loop type to cycle, pingpong, offset, or continue.
//
// Use:
// Select your keyframed properties and run this script to apply the
// expression. Default loop type is cycle. Hold alt for contintue. Hold shift
// for offset. Hold control for pingpong.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Apply Loop Both Expression");

        var altKey = ScriptUI.environment.keyboardState.altKey;
        var shiftKey = ScriptUI.environment.keyboardState.shiftKey;
        var ctrlKey = ScriptUI.environment.keyboardState.ctrlKey;

        var type = "cycle";
        if (altKey === true && shiftKey === false && ctrlKey === false) {
            type = "continue";
        } else if (altKey === false && shiftKey === true && ctrlKey === false) {
            type = "offset";
        } else if (altKey === false && shiftKey === false && ctrlKey === true) {
            type = "pingpong";
        }

        var comp = app.project.activeItem;
        var properties = comp.selectedProperties;
        var numProperties = properties.length;
        for (var p = 0; p < numProperties; p++) {
            var property = properties[p];
            if (property.canSetExpression === true) {
                var propertyValueType = property.propertyValueType;
                if (propertyValueType !== PropertyValueType.SHAPE) {
                    property.expression = "loopIn(\"" + type + "\") + loopOut(\"" + type + "\") - value;";
                }
            }
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
