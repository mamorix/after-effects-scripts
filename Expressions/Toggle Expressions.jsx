// Toggle Expressions.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Toggle Expressions
// Version: 1.0
//
// Description:
// TJ Peters asked for a version of this script to work with KBar. Sometimes
// you need to disable or enable expressions for all the selected properties
// in a composition.
//
// Use:
// Select any properties with expressions and run the script. Click to disable
// the expressions. Alt + click to enable the expressions.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Toggle Expressions");
        var altKey = ScriptUI.environment.keyboardState.altKey;
        var comp = app.project.activeItem;
        var properties = comp.selectedProperties;
        var numProperties = properties.length;
        for (var p = 0; p < numProperties; p++) {
            var property = properties[p];
                property.expressionEnabled = altKey;
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
