// New Script Template.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: New Script Template
// Version: 1.0
//
// Description:
// ...
//
// Use:
// ...
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("");
        var comp = app.project.activeItem;
        var layers = comp.layers;
        var numLayers = layers.length;
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
