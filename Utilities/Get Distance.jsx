// Get Distance.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Get Distance
// Version: 1.0
//
// Description:
// Get the distance between two layers.
//
// Use:
// Select two layers and run the script to get the distance.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        var comp = app.project.activeItem;
        var layers = comp.selectedLayers;
        var numLayers = layers.length;
        if (numLayers !== 2) {
            alert("Select 2 layers");
        } else {
            var layerA = layers[0];
            var layerB = layers[1];

            var a = layerA.transform.position.value;
            var b = layerB.transform.position.value;

            var distance = Math.sqrt(Math.pow(b[0] - a[0], 2) + Math.pow(b[1] - a[1], 2));

            var layerAMsg = "[" + layerA.index + "] " + layerA.name;
            var layerBMsg = "[" + layerB.index + "] " + layerB.name;

            alert(distance.toFixed(2) + "px\n" + layerAMsg + "\n" + layerBMsg);
        }
    } catch(err) {
        alert(err);
    }
})();
