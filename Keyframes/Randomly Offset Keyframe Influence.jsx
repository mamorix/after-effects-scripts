// Randomly Offset Keyframe Influence.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Randomly Offset Keyframe Influence
// Version: 1.1
//
// Description:
// Add a little variation to your keyframe influence to help keep your
// animations feel more "organic." Ugh, what am I, a creative director?
//
// Use:
// Select your keyframes(s), run this script, and enter your influence range.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    function getRandomNumber(min, max) {
        return Math.random() * (max - min) + min;
    }

    function offsetInfluence(influence, range) {
        var offset = getRandomNumber(-range, range);
        var influence = influence + offset;
            influence = (influence > 100) ? 100 : influence;
            influence = (influence <= 0) ? 0.1 : influence;
        return influence;
    }

    function getNewKeyframeEase(keyframeEase, range) {
        var influence = offsetInfluence(keyframeEase.influence, range);
        return new KeyframeEase(0, influence);
    }

    try {
        app.beginUndoGroup("Randomly Offset Keyframe Influence");
        var comp = app.project.activeItem;
        var properties = comp.selectedProperties;
        var numProperties = properties.length;
        var range = parseFloat(prompt("Influence Range (Percentage)", ""));
        for (var p = 0; p < numProperties; p++) {
            var property = properties[p];
            var keys = property.selectedKeys;
            var numKeys = keys.length;
            for (var k = 0; k < numKeys; k++) {
                var keyIndex = keys[k];

                var currentInTemporalEase = property.keyInTemporalEase(keyIndex);
                var newInTemporalEase = [];
                var numInEase = currentInTemporalEase.length;
                for (var i = 0; i < numInEase; i++) {
                    var keyframeEase = getNewKeyframeEase(currentInTemporalEase[i], range);
                    newInTemporalEase.push(keyframeEase);
                }

                var currentOutTemporalEase = property.keyOutTemporalEase(keyIndex);
                var newOutTemporalEase = [];
                var numOutEase = currentOutTemporalEase.length;
                for (var o = 0; o < numOutEase; o++) {
                    var keyframeEase = getNewKeyframeEase(currentOutTemporalEase[o], range);
                    newOutTemporalEase.push(keyframeEase);
                }

                property.setTemporalEaseAtKey(keyIndex, newInTemporalEase, newOutTemporalEase);
            }
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
