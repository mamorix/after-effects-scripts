// Randomly Offset Keyframe Values.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Randomly Offset Keyframe Values
// Version: 1.1
//
// Description:
// Add a little variation to your keyframe values to help keep your
// animations feel more "organic." Ugh, what am I, a creative director?
//
// Use:
// Select your keyframes(s), run this script, and enter your influence values.
//
// Note:
// This only works for one dimensional properties right now. I know... lame.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    function getRandomNumber(min, max) {
        return Math.random() * (max - min) + min;
    }

    function offsetValue(value, range) {
        var offset = getRandomNumber(-range, range);
        var newValue = value + offset;
        return newValue;
    }

    function getNewKeyframeValue(value, range) {
        var newValue = offsetValue(value, range);
        return newValue;
    }

    try {
        app.beginUndoGroup("Randomly Offset Keyframe Values");
        var comp = app.project.activeItem;
        var properties = comp.selectedProperties;
        var numProperties = properties.length;
        var range = parseFloat(prompt("Value Range (Units)", ""));
        for (var p = 0; p < numProperties; p++) {
            if (property.propertyValueType === PropertyValueType.OneD) {
                var property = properties[p];
                var keys = property.selectedKeys;
                var numKeys = keys.length;
                for (var k = 0; k < numKeys; k++) {
                    var keyIndex = keys[k];
                    var keyframeValue = property.keyValue(keyIndex);
                    var newKeyframeValue = getNewKeyframeValue(keyframeValue, range);
                    property.setValueAtKey(keyIndex, newKeyframeValue);
                }
            }
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
