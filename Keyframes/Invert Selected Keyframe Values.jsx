// Invert Selected Keyframe Values.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Invert Selected Keyframe Values
// Version: 1.0
//
// Description:
// Sometimes you need to reverse an overshoot or just invert how keyframes were
// previously animated.
//
// Use:
// Select your keyframes and run this script invert their values.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Invert Selected Keyframe Values");
        var comp = app.project.activeItem;
        var properties = comp.selectedProperties;
        var numProperties = properties.length;
        for (var p = 0; p < numProperties; p++) {
            var property = properties[p];
            if (property.propertyType == PropertyType.PROPERTY && property.canVaryOverTime) {
                var keyframes = property.selectedKeys;
                var numKeyframes = keyframes.length;
                for (var k = 0; k < numKeyframes; k++) {
                    var keyframe = keyframes[k];
                    var value = property.keyValue(keyframe) * -1;
                    property.setValueAtKey(keyframe, value);
                }
            }
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
