// Background Toggle.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Background Toggle
// Version: 1.0
//
// Description:
// Toggle the background color of the active composition between black and
// 50% grey. No modifier needed.
//
// Use:
// Click to toggle the background color of the active composition between black
// and 50% grey. No modifier needed.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Background Toggle");
        var comp = app.project.activeItem;
        comp.bgColor = (comp.bgColor[0] === 0) ? [0.5,0.5,0.5] : [0,0,0];
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
