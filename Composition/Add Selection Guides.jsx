// Add Selection Guides.jsx
// Copyright (c) 2016-2019 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Add Selection Guides
// Version: 1.0
//
// Description:
// Add guides at the anchor points of all selected layers.
//
// Use:
// Click to remove all guides and add new guides. Alt click to keep old guides.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Add Selection Guides");    
        var altKey = ScriptUI.environment.keyboardState.altKey;
        var comp = app.project.activeItem;
        if (altKey === false) {
            while (comp.guides.length > 0) {
                comp.removeGuide(0);
            }
        }
        var selectedLayers = comp.selectedLayers;
        var numLayers = selectedLayers.length;
        for (var l = 0; l < numLayers; l++) {
            var layer = selectedLayers[l];
            var pointControl = layer.property("ADBE Effect Parade").addProperty("ADBE Point Control");
            var pointProperty = pointControl.property("ADBE Point Control-0001");
            pointProperty.expression = "toComp(transform.anchorPoint);";
            var xPosition = pointProperty.value[0];
            var yPosition = pointProperty.value[1];
            comp.addGuide(1,xPosition);
            comp.addGuide(0,yPosition);
            pointControl.remove();
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
