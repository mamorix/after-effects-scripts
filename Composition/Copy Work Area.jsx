// Coopy Work Area.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Copy Work Area
// Version: 1.0
//
// Description:
// Ryan Summers requested a way to copy and paste the current Work Area to
// another composition. I gave it a try but soon realized that standalone
// scripts don't exactly have a memory. Zack Lovatt suggested using After
// Effects preferences to store things. What a genius.
//
// Use:
// Run this script with an active composition to copy the Work Area in and out.
// Select a new composition and run this script while holding the ALT key to
// paste the Work Area in and out.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    function copyWorkAreaFromComp(comp) {
        app.settings.saveSetting("KM Copy Work Area", "Start", comp.workAreaStart);
        app.settings.saveSetting("KM Copy Work Area", "Duration", comp.workAreaDuration);
        app.preferences.saveToDisk();
    }

    function pasteWorkAreaToComp(comp) {
        if (app.settings.haveSetting("KM Copy Work Area", "Start") === true) {
            comp.workAreaStart = app.settings.getSetting("KM Copy Work Area", "Start");
            comp.workAreaDuration = app.settings.getSetting("KM Copy Work Area", "Duration");
        }
    }

    try {
        app.beginUndoGroup("Copy/Paste Work Area");
        var comp = app.project.activeItem;
        var altKey = ScriptUI.environment.keyboardState.altKey;
        if (altKey === true) {
            pasteWorkAreaToComp(comp);
        } else {
            copyWorkAreaFromComp(comp);
        }
    } catch (err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();