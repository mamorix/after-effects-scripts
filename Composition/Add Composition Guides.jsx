// Add Composition Guides.jsx
// Copyright (c) 2016-2019 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Add Composition Guides
// Version: 1.0
//
// Description:
// Add composition guides to the center and edges of the composition.
//
// Use:
// Click to remove all guides and add new guides. Alt click to keep old guides.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Add Composition Guides");  
        var comp = app.project.activeItem;  
        var altKey = ScriptUI.environment.keyboardState.altKey;
        if (altKey === false) {
            while (comp.guides.length > 0) {
                comp.removeGuide(0);
            }
        }
        var fullHeight = comp.height;
        var fullWidth = comp.width;
        comp.addGuide(0,0);
        comp.addGuide(0,fullHeight);
        comp.addGuide(1,0);
        comp.addGuide(1,fullWidth);
        var halfHeight = fullHeight / 2;
        var halfWidth = fullWidth / 2;
        comp.addGuide(0,halfHeight);
        comp.addGuide(1,halfWidth);
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
