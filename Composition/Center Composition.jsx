// Center Composition.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Center Composition
// Version: 1.1
//
// Description:
// Scrolling, dragging, and zooming your composition back to a size that fits
// the composition viewer is terrible. Autmatically centering your composition
// is wonderful.
//
// Use:
// Run this script to center your your composition in the viewer at 50% zoom.
// 50% is generally perfect for a 1080p composition. Hold ALT to zoom to 100%.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    var altKey = ScriptUI.environment.keyboardState.altKey;
    var shiftKey = ScriptUI.environment.keyboardState.shiftKey;
    var zoom = 1;
    if (altKey && !shiftKey) {
        zoom = 0.5;
    } else if (!altKey && shiftKey) {
        zoom = 0.25;
    }
    app.activeViewer.views[0].options.zoom = zoom;
})();
