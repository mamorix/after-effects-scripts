// Reset Work Area.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Reset Work Area
// Version: 1.0
//
// Description:
// Reset the work area to cover the entire composition.
//
// Use:
// Run the script to set the work area in to the beginning of the composition
// and the work area out to the end of the composition.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Reset Work Area");
        var comp = app.project.activeItem;
        comp.workAreaStart = 0;
        comp.workAreaDuration = comp.duration;
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
