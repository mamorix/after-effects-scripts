// Force Refresh.jsx
// Copyright (c) 2016-2019 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Force Refresh
// Version: 1.0
//
// Description:
// After Effects CC 2019 doesn't always update the Composition Panel when
// changes are made. Simple click to force a refresh.
//
// Use:
// Click to force a refresh.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Force Refresh");
        var comp = app.project.activeItem;
        comp.motionBlur = !comp.motionBlur;
        comp.motionBlur = !comp.motionBlur;
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
