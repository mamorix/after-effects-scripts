// Toggle Onion Skinning.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Toggle Onion Skinning
// Version: 1.1
//
// Description:
// After Effects doesn't have true native onion skinning but using CC Wide Time
// works pretty well. Fun fact time! "cc969d6ad230c29fc7fb91db0030e5df" is the
// string that gets added to the layer comment to help identify it later. That
// string is actually the md5 hash for "onionskin" and yes, I'm a nerd.
//
// Use:
// Run the script to toggle onion skinning. No need to hold ALT. The script
// will figure it out automatically.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function(){
    function enableOnionSkin(comp) {
        try {
            app.beginUndoGroup("Enable Onion Skin");
            var layer = comp.layers.addSolid([0,0,0], "Onion Skin", comp.width, comp.height, 1);
            layer.adjustmentLayer = true;
            layer.comment = "cc969d6ad230c29fc7fb91db0030e5df";
            layer.label = 0;
            layer.moveToBeginning();
            var effect = layer.property("ADBE Effect Parade").addProperty("CC Wide Time");
            effect.name = "Onion Skin";
        } catch(err) {
            alert(err);
        } finally {
            app.endUndoGroup();
        }
    }

    function disableOnionSkin(layer) {
        try {
            app.beginUndoGroup("Disable Onion Skin");
            layer.source.remove();
        } catch(err) {
            alert(err);
        } finally {
            app.endUndoGroup();
        }
    }

    try {
        var comp = app.project.activeItem;
        var layers = comp.layers;
        var numLayers = layers.length;

        var onionSkinningEnabled = false;
        for (var i = 1; i <= numLayers; i++) {
            var layer = layers[i];
            var comment = layer.comment;
            if (comment === "cc969d6ad230c29fc7fb91db0030e5df") {
                onionSkinningEnabled = true;
                break;
            }
        }

        if (onionSkinningEnabled === false) {
            enableOnionSkin(comp);
        } else if (onionSkinningEnabled === true) {
            disableOnionSkin(layer);
        }
    } catch(err) {
        alert(err);
    }
})();
