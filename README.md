### After Effects Scripts
#### Version: 1.0
##### Copyright (c) 2016-2019 Kyle Martinez (kyletmartinez@gmail.com). All rights reserved.

These scripts are provided "as is," without warranty of any kind, expressed or implied. In no event shall the author be held liable for any damages arising in any way from the use of these scripts.

In other words, I'm just trying to help make life as an animator easier.

*"A rising tide lifts all boats."* - JKF, 1963

-----------------------
##### Description

A collection of helpful scripts for After Effects. All of these scripts can be launched natively using *File > Scripts > Run Script File...* in the After Effects menu or by using a 3rd party script launcher.

* [KBar2](https://aescripts.com/kbar/ "KBar on aescripts")
* [Quick Menu 2](https://aescripts.com/quick-menu/ "Quick Menu 2 on aescripts")
* [Tool Launcher](https://aescripts.com/tool-launcher/ "Tool Launcher on aescripts")

-----------------------
##### Composition
* **Add Composition Guides:** Add guides to the center and edge of the composition.
* **Add Selection Guides"** Add guides to the anchor points of all selected layers.
* **Background Toggle:** Quickly toggle the background color of the current composition between black and 50% grey.
* **Center Composition:** Click to center your composition and zoom to 50%. Alt + click to center and zoom to 100%.
* **Copy Work Area:** Click to copy the work area from your current composition. Alt + click to paste that work area to another composition.
* **Force Refresh:** Force After Effects to redraw the Composition Panel. Especially helpful in CC 2019.
* **Reset Work Area:** Reset the work area to cover the entire composition duration.
* **Toggle Onion Skinning:** CC Wide Time works pretty well for onion skinning. No need to hold ALT. The script will figure out to toggle on/off automatically.

##### Effects
* **Make Sticky Effect:** Effects such as Gradient Ramp or CC Bend It don't properly stick to their layer when animating. Now they will.
* **Type Array Obliterator:** Type Array by Paul Slemmer and Paul Conigs is amazing. Its really powerful and extremely easy to apply the preset. Removing the preset though? Tough. Now you can completely remove Type Array while keeping your original text intact.

##### Expressions
* **Apply Loop Both Expression:** Applies an expression to each selected property (if it can) that will loop keyframes backwards and forwards.
* **Apply Loop In Expression:** Applies an expression to each selected property (if it can) that will loop keyframes backwards.
* **Apply Loop Out Expression:** Applies an expression to each selected property (if it can) that will loop keyframes forewards.
* **Apply Maintain Stroke Weight Expression:** Applies an expression to each selected stroke weight property that will maintain weight when scaling and parented.
* **Posterize Property:** Apply an expression to all selected properties to posterize their values. Hold at for fps popup.
* **Toggle Expressions:** Sometimes you need to disable or enable expressions for all the selected properties in a composition. Click to disable. Alt + click to enable.

##### Keyframes
* **Invert Selected Keyframe Values:** Inver the selected keyframe values.
* **Randomly Offset Keyframe Influence:** Randomly offset the influence for all selected keyframes.
* **Randomly Offset Keyframe Values:** Randomly offset the values for all selected keyframes (1 dimensional properties only right now).

##### Layers
* **Add Text Layers:** Add a new text layer for every line of an external text document.
* **Disable Track Mattes:** Disable all track mattes in the current composition.
* **Hard Solo:** Exactly like solo but uses the eyeball icon to disable all layers except for those selected.
* **Lock All Layers:** Lock every layer in every composition in the active project. Hold ALT to unlock every layer.
* **Move Layer To Midpoint:** Move a layer to the midpoint between two other layers.
* **Move Selected Layers To Bottom:** Sometimes you just need to move all selected layers to the Bottom of the timeline.
* **Move Selected Layers To Top:** Sometimes you just need to move all selected layers to the Top of the timeline.
* **Randomly Offset Layer Start Time:** Randomly offset the start time for all selected layers by +/- 1 second.
* **Rename Layers:** Rename the selected layers and append a number to the end delimited with a hyphen.
* **Replace Single Layer With Precomp:** Replace the selected layer and all other instances with a precomp. Will enabled Collapse Transformation by default.
* **Select All Un-Parented:** Select all layers in the current composition that do not have a parent. Hold ALT to only select those orphan layers active at the current time.
* **Shift Layers:** Shift all the selected layers to the Current Time Indicator.
* **Split Layers:** Exact same behavior as running Edit > Split Layer (Shift + Command + D) with the added benefit of removing the track matte.
* **Zero Position:** Select a single layer and zero out it's position while keeping your project and composition clean.

##### Markers
* **Add Marker:** Add a marker to selected layers with comment or hold ALT to add a marker to the composition (CC 2017+ only).

##### Project
* **Clean Render Queue:** Clean out the render queue. Especially nice when importing another project.
* **Clean Selected Folder:** Remove unused items from the selected folder (currently ignores subfolders).
* **Match Source Name to Layer Name:** Rename a selected project item (such as "Black Solid 1") to match its name in the composition (such as "Particular"). Useful for keeping once-used solids organized.

##### Properties
* **Basic Hose Maker:** Recreate a stroke and controller position quickly and easily when selecting a new RubberHose and a shape layer. Doesn't matter what order you select them in.
* **Cel Shader:** In daily 2D animation world I often want to add a shadow or a highlight using the same color but in a blend mode such as multiply/screen and with reduced opacity. Usually this is fine but when multiple shadows overlap things get weird. This script will calculate what the final color will be and then just apply it directly.
* **Get Average Position:** Move the last selected layer to the average position of all other selected layers in composition space.
* **Link Property To Layer:** Link a position-like property to a layer using the toComp() method. Just select a layer and the property you want to link. Doesn't matter what order you select them in.
* **Separate Dimensions:** Separate the position dimensions of selected layers without needing to select the position property. Alt + Click to reconnect them.

##### Template
* **New Script Template:** Basic template for all of these small utility scripts.

##### Utilities
* **Get Distance:** Get the distance between two layers.