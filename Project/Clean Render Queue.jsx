// Clear Render Queue".jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: CClear Render Queue"
// Version: 1.0
//
// Description:
// Clean the render queue. Expecially useful after importing another project.
//
// Use:
// Run the script the clean the render queue.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Clear Render Queue");
        while (app.project.renderQueue.numItems > 0) {
            app.project.renderQueue.item(app.project.renderQueue.numItems).remove();
        }
    } catch (err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();