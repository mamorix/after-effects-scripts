// Match Source Name to Layer Name.jsx
// Copyright (c) 2016-2019 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Match Source Name to Layer Name
// Version: 1.1
//
// Description:
// Rename a selected project item (such as "Black Solid 1") to match its name
// in the composition (such as "Particular"). Useful for keeping once-used
// solids organized.
//
// Use:
// Select the items you want to rename and click to match the name. If the item
// is used more than once it will be ignored.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    function findLayer (source, comp) {
        var layers = comp.layers;
        var numLayers = layers.length;
        for (var l = numLayers; l > 0; l--) {
            var layer = layers[l];
            if (layer.source === source) {
                break;
            }
        }
        return layer;
    }

    try {
        app.beginUndoGroup("Match Source Name to Layer Name");
        var items = app.project.selection;
        var numItems = items.length;
        for (var i = 0; i < numItems; i++) {
            var item = items[i];
            if (item.usedIn.length === 1) {
                var containingComp = item.usedIn[0];
                var layer = findLayer(item, containingComp);
                item.name = layer.name;
            }
        }
    } catch (err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();