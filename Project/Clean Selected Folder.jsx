// Clean Selected Folder.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Clean Selected Folder
// Version: 1.1
//
// Description:
// Clean unused items from the selected folder. Doing it manually is rather
// tedious since an item that was previously used might not be after cleaning
// the folder once. This script runs recursively until the folder is truely
// clean. Well, almost... it currently ignores any subfolders.
//
// Use:
// Select a single folder and run this script to clean out any unused items.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    function cleanSelectedFolder(folder) {
        var itemsCleaned = 0;
        var items = folder.items;
        var numItems = folder.numItems;
        for (var i = numItems; i > 0; i--) {
            var item = items[i];
            if (!(item instanceof FolderItem)) {
                if (item.usedIn.length === 0) {
                    item.remove();
                    itemsCleaned++;
                }
            }
        }
        return (itemsCleaned === 0);
    }

    try {
        app.beginUndoGroup("Clean Selected Folder");
        var folder = app.project.selection[0];
        while (!cleanSelectedFolder(folder));
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
