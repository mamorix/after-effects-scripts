// Add Marker.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Add Marker
// Version: 1.1
//
// Description:
// Adding a marker with a comment to the composition or selected layer is
// annoying. You have to add a marker, double click it, and type your comment.
// Then, because hittin the enter key adds a new line to the marker comment
// instead closing the new marker popup, you have to click "Ok."
//
// What if you want to add the same marker with the same comment to multiple
// layers? Too bad.
//
// Edit: I recently learned you can hit the enter key found in the number pad
// area on your keyboard to close the new marker popup.
//
// Use:
// Select your layer(s) and run the script to add a marker with a comment at
// the Current Time Indicator (CTI). Hold ALT to add a marker with a comment to
// your current composition at the CTI. Just hit enter again while in the
// comment prompt to add a marker without a comment.
//
// Note:
// You can only add composition markers in version 14.0 (CC 2017) and newer.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Add Marker");
        var altKey = ScriptUI.environment.keyboardState.altKey;
        var comp = app.project.activeItem;
        if (altKey === true) {
            var comment = prompt("Marker Comment", "");
            var marker = new MarkerValue(comment);
            comp.markerProperty.setValueAtTime(comp.time, marker);
        } else {
            var layers = comp.selectedLayers;
            var numLayers = layers.length;
            if (numLayers > 0) {
                var comment = prompt("Marker Comment", "");
                var marker = new MarkerValue(comment);
                for (var l = 0; l < numLayers; l++) {
                    var layer = layers[l];
                    layer.marker.setValueAtTime(comp.time, marker);
                }
            }
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
