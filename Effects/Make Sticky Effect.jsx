// Make Sticky Effect.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Make Sticky Effect
// Version: 1.0
//
// Description:
// This script converts selected effects to "sticky effects." Effects such as
// Gradient Ramp or CC Bend It don't properly stick to their layer when
// animating. Now they will.
//
// Use:
// Select your effect(s) and run this script to make them stick.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    function isSpacialProperty(property) {
        switch(property.propertyValueType) {
            case PropertyValueType.TwoD_SPATIAL:
                return true;
            case PropertyValueType.ThreeD_SPATIAL:
                return true;
            default:
                return false;
        }
    }

    try {
        app.beginUndoGroup("Make Sticky Effect");
        var comp = app.project.activeItem;
        var effects = comp.selectedProperties;
        var numEffects = effects.length;
        for (var e = 0; e < numEffects; e++) {
            var effect = effects[e];
            if (effect.propertyGroup(1).matchName === "ADBE Effect Parade") {
                var numProperties = effect.numProperties;
                for (var p = 1; p <= numProperties; p++) {
                    var property = effect.property(p);
                    if (isSpacialProperty(property) === true) {
                        property.expression = "toComp(anchorPoint + value);";
                        var oldValue = property.value;
                        var newValue = oldValue - [comp.width, comp.height];
                        property.setValue(newValue);
                    }
                }
            }
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
