// Type Array Obliterator.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Type Array Obliterator
// Version: 1.1
//
// Description:
// Type Array by Paul Slemmer and Paul Conigs is amazing. Its really powerful
// and extremely easy to apply the preset. Removing the preset though? Tough.
//
// Use:
// Select your text layers and run the script to completely remove Type Array
// while keeping your original text intact.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        var comp = app.project.activeItem;
        var layers = comp.selectedLayers;
        var numLayers = layers.length;
        for (var l = 0; l < numLayers; l++) {
            var layer = layers[l];
            try {
                var animator = layer.property("ADBE Text Properties").property("ADBE Text Animators").property("ADBE Text Animator");
                if (animator.name === "Type Array Animator") {
                    animator.remove();
                    layer.property("ADBE Effect Parade").property("Pseudo/Type_Array").remove();
                    try {
                        layer.property("ADBE Mask Parade").property("Mask 1").remove();
                    } catch(err) {
                        // must not have a mask applied
                    }
                }
            } catch (err) {
                // must not have Type Array applied
            }
        }
    } catch (err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
