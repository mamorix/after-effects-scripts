// Select All Un-Parented.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Select All Un-Parented
// Version: 1.1
//
// Description:
// This script will select all layers in the current composition that do not
// have a parent. Hold ALT to only select those orphan layers active at the
// current time.
//
// Use:
// Run this script to select all layers in the current composition that do not
// have a parent. Hold ALT to only select those orphan layers active at the
// current time.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Select All Un-Parented");
        var altKey = ScriptUI.environment.keyboardState.altKey;
        var comp = app.project.activeItem;
        var layers = comp.layers;
        var numLayers = layers.length;
        for (var l = 1; l <= numLayers; l++) {
            var layer = layers[l];
            if (layer.parent === null) {
                if (altKey === true) {
                    var time = comp.time;
                    var inPoint = layer.inPoint;
                    var outPoint = layer.outPoint;
                    if (time >= inPoint && time <= outPoint) {
                        layer.selected = true;
                    }
                } else {
                    layer.selected = true;
                }
            }
        }
    } catch (err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();