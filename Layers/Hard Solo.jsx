// Hard Solo.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Hard Solo
// Version: 1.0
//
// Description:
// Solo temporarily disables the visibilty of all other layers in the
// composition. Hard Solo works exactly like Solo but using the eyeball icon.
//
// Use:
// Select a layer or layers and run this script. Every layer but the selected
// layer(s) will be disabled (the eyeball icon will be toggled off).
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Hard Solo");
        var comp = app.project.activeItem;
        var layers = comp.layers;
        var numLayers = layers.length;
        for (var l = 1; l <= numLayers; l++) {
            var layer = layers[l];
            layer.enabled = layer.selected;
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
