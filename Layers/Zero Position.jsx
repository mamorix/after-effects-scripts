// Zero Position.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Zero Position
// Version: 1.1
//
// Description:
// Its really nice to zero out the position values for layers especially for
// character animation. This script will automate the entire process while
// keeping your project completely organized. Unlike DuIK, Zero Position will
// only ever create 1 null. It will reuse that null everytime you zero out
// a position value.
//
// Use:
// Select a single layer and run the script to zero out it's position.
//
// Note:
// Thank you Tomas Šinkūnas for your help with recurisively searching the
// project panel.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    function findProjectItems(folder, items) {
        items = items || [];
        var numFolderItems = folder.items.length;
        for (var i = numFolderItems; i > 0; i--) {
            var folderItem = folder.items[i];
            if (folderItem.name === "Zero Position") {
                items.push(folderItem);
            } else if (folderItem instanceof FolderItem && folderItem.numItems > 0) {
                findProjectItems(folderItem, items);
            }
        }
        return items;
    }

    function findZeroNullSource() {
        var folder = app.project.rootFolder;
        var items = findProjectItems(folder);
        return (items.length === 0) ? false : items[0];
    }

    function findZeroNullLayer(sourceItem, comp) {
        var layers = comp.layers;
        var numLayers = layers.length;
        for (var l = numLayers; l > 0; l--) {
            var layer = layers[l];
            if (layer.source === sourceItem) {
                break;
            }
        }
        return layer;
    }

    try {
        app.beginUndoGroup("Set Zero Position");
        var comp = app.project.activeItem;
        if (comp == null || !(comp instanceof CompItem)) {
            throw "You must open a composition.";
        }
        var layers = comp.selectedLayers;
        var numLayers = layers.length;
        if (numLayers !== 1) {
            throw "You must select a single layer.";
        }
        var originalLayer = layers[0];
        var originalPositionProperty = originalLayer.transform.position;
        if (originalPositionProperty.numKeys > 0) {
            var originalPosition = originalPositionProperty.keyValue(1);
        } else {
            var originalPosition = originalPositionProperty.value;
        }
        var zeroNullSource = findZeroNullSource();
        if (zeroNullSource) {
            var containingComp = zeroNullSource.usedIn[0];
            var tempLayerOne = findZeroNullLayer(zeroNullSource, containingComp);
            var tempLayerTwo = tempLayerOne.duplicate();
            tempLayerTwo.locked = false;
            tempLayerTwo.parent = null;
            tempLayerTwo.copyToComp(comp);
            tempLayerTwo.remove();
            var zeroNull = comp.layer(originalLayer.index - 1);
        } else {
            var zeroNull = comp.layers.addNull();
            zeroNull.source.name = "Zero Position";
        }
        zeroNull.name = "z_" + originalLayer.name;
        zeroNull.transform.position.setValue(originalPosition);
        originalLayer.parent = zeroNull;
        zeroNull.moveToEnd();
        zeroNull.enabled = false;
        zeroNull.guideLayer = true;
        zeroNull.label = 0;
        zeroNull.locked = true;
        zeroNull.shy = true;
        comp.hideShyLayers = true;
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
