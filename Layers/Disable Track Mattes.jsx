// Disable Track Mattes.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Disable Track Mattes
// Version: 1.1
//
// Description:
// This script will disable every layer (turn eyeball icon off) that is a track
// matte in the current composition. Especially helpful after painting the
// video column (dragging all the eyes open).
//
// Use:
// With a composition open, run the script and every track matte will be
// disabled.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Disable Track Mattes");
        var comp = app.project.activeItem;
        var layers = comp.layers;
        var numLayers = layers.length;
        for (var l = 1; l <= numLayers; l++) {
            var layer = layers[l];
            if (layer.isTrackMatte === true) {
                layer.enabled = false;
            }
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
