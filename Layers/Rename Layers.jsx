// Rename Layers.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Rename Layers
// Version: 1.3
//
// Description:
// Quicky rename all selected layers with sequential numbers. Numbers will
// always be padded with zeros and have a minimum of two digits.
//
// Use:
// Select your layer(s) and run the script to rename the selected layers.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    function padNumber(num, length) {
        return Array(Math.max(length - String(num).length + 1, 0)).join(0) + num;
    }

    try {
        app.beginUndoGroup("Rename Layers");
        var comp = app.project.activeItem;
        var layers = comp.selectedLayers;
        var numLayers = layers.length;
        if (numLayers > 0) {
            var name = prompt("Name", "");
            var length = Math.max(numLayers.toString().length, 2);
            for (var l = 0; l < numLayers; l++) {
                var layer = layers[l];
                var number = l + 1;
                    layer.name = name + " " + padNumber(number, length);
            }
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
