// Add Text Layers.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Add Text Layers
// Version: 1.1
//
// Description:
// This script will add a new text layer for every line found in an external
// text document.
//
// Use:
// Run this script, select a text document, and a new text layer will be added
// for every line of text.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Add Text Layers");
        var comp = app.project.activeItem;
        var layers = comp.layers;
        var file = File.openDialog();
        file.open("r");
        var contents = file.read();
        file.close();
        var lines = contents.split("\n");
        var numLines = lines.length;
        for (var l = 0; l < numLines; l++) {
            layers.addText(lines[l]);
        }
    } catch (err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();