// Randomly Offset Layer Start Time.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Randomly Offset Layer Start Time
// Version: 1.1
//
// Description:
// Add a little variation to your layer start time to help keep your animations
//  feel more "organic." Ugh, what am I, a creative director?
//
// Use:
// Select your layer(s) and run this script. Default range is +/- 1 second.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Randomly Offset Layer Start Time");
        var comp = app.project.activeItem;
        var layers = comp.selectedLayers;
        var numLayers = layers.length;
        var min = -1;
        var max = 1;
        var fps = comp.frameRate;
        for (var l = 0; l < numLayers; l++) {
            var offset = Math.random() * (max - min) + min;
                offset = Math.round(offset * fps) / fps;
            var layer = layers[l];
                layer.startTime += offset;
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
