// Shift Layers.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Shift Layers
// Version: 1.1
//
// Description:
// Using ALT + [ to shift a layer to the Current Time Indicator is very
// convenient. Imagine being able to do that for a group of selected layers.
//
// Use:
// Select your layer(s) and run the script to shift the entire group of
// selected layers to the Current Time Indicator.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Shift Layers");
        var comp = app.project.activeItem;
        var layers = comp.selectedLayers;
        var numLayers = layers.length;
        var inPoint = layers[0].inPoint;
        for (var l = 1; l < numLayers; l++) {
            var layer = layers[l];
            if (layer.inPoint < inPoint) {
                inPoint = layer.inPoint;
            }
        }
        var difference = comp.time - inPoint;
        for (var l = 0; l < numLayers; l++) {
            var layer = layers[l];
            layer.startTime += difference;
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
