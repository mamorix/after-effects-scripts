// Move Layer To Midpoint.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Move Layer To Midpoint
// Version: 1.0
//
// Description:
// Move a layer to the midpoint between two other layers.
//
// Use:
// Select three layers and run the script. The third layer selected will move
// to the midpoint between the first two layers selected.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Move Layer To Midpoint");
        var comp = app.project.activeItem;
        var layers = comp.selectedLayers;

        var positionA = layers[0].transform.position.value;
        var positionB = layers[1].transform.position.value;

        var positionX = (positionA[0] + positionB[0]) / 2;
        var positionY = (positionA[1] + positionB[1]) / 2;

        layers[2].transform.position.setValue([positionX, positionY]);
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();