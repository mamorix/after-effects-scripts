// Replace Single Layer With Precomp.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Replace Single Layer With Precomp
// Version: 1.1
//
// Description:
// Originally built to help with Cartoon Moblur. This script will replace the
// selected layer with a precomposed version of itself. It will also replace
// any instance of itself in any other composition in the project.
//
// Use:
// Select a layer and run this script to replace it and all other instances
// with a precomp. Will enabled Collapse Transformation by default.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Single Replace With Precomp");

        var currentProject = app.project;
        var currentComp = currentProject.activeItem;
        var currentLayer = currentComp.selectedLayers[0];
        var currentSource = currentLayer.source;
        var currentCompsUsedIn = currentSource.usedIn;

        var name = currentSource.name;
        var width = currentSource.width;
        var height = currentSource.height;
        var pixelAspect = currentSource.pixelAspect;
        var duration = currentComp.duration;
        var frameRate = currentComp.frameRate;

        var newComp = currentProject.items.addComp(name, width, height, pixelAspect, duration, frameRate);
            newComp.layers.add(currentSource);

        var numComps = currentCompsUsedIn.length;
        for (var c = 0; c < numComps; c++) {
            var comp = currentCompsUsedIn[c];
            var layers = comp.layers;
            var numLayers = comp.numLayers;
            for (var l = 1; l <= numLayers; l++) {
                var layer = layers[l];
                if (layer.source === currentSource) {
                    layer.replaceSource(newComp, true);
                    layer.collapseTransformation = true;
                }
            }
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
