// Lock All Layers.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Lock All Layers
// Version: 1.1
//
// Description:
// This script will lock every layer in every composition found in the current
// project. Hold ALT to unlock them all.
//
// Use:
// With a project open, run the script and every layer will be locked. Hold ALT
// to unlock every layer.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Lock All Layers");
        var altKey = ScriptUI.environment.keyboardState.altKey;
        var project = app.project;
        var items = project.items;
        var numItems = items.length;
        for (var i = 1; i <= numItems; i++) {
            var item = items[i];
            if (item instanceof CompItem) {
                var layers = item.layers;
                var numLayers = layers.length;
                for (var l = 1; l <= numLayers; l++) {
                    var layer = layers[l];
                    layer.locked = (altKey === true) ? false : true;
                }
            }
        }
    } catch (err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
