// Move Selected Layers To Top.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Move Selected Layers To Top
// Version: 1.1
//
// Description:
// Sometimes you just need to jump all the selected layers to the top of the
// timeline.
//
// Use:
// Select your layer(s) and run the script to move all selected layers to the
// top of the timeline.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    function compareLayerIndex(a, b) {
        return a.index - b.index;
    }

    try {
        app.beginUndoGroup("Move Selected Layers To Top");
        var comp = app.project.activeItem;
        var layers = comp.selectedLayers.sort(compareLayerIndex);
            layers.reverse();
        var numLayers = layers.length;
        for (var l = 0; l < numLayers; l++) {
            var layer = layers[l];
            layer.moveToBeginning();
        }
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
