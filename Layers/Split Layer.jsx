// Split Layer.jsx
// Copyright (c) 2016-2018 Kyle Martinez. All rights reserved.
// www.kyle-martinez.com
//
// Name: Split Layer
// Version: 1.0
//
// Description:
// When you split a layer that currently has a track matte, the resulting layer
// will also be looking for a track matte. That behavior usually isn't desired
// and is especially frustrating when you don't remember it happens.
//
// Use:
// Select a single layer and run the script. After Effects will behave the
// exact same way as running Edit > Split Layer (Shift + Command + D) with
// the added benefit of removing the track matte.
//
// Legal stuff:
// This script is provided "as is," without warranty of any kind, expressed or
// implied. In no event shall the author be held liable for any damages
// arising in any way from the use of this script.
//
// In other words, I'm just trying to help make life as an animator easier.
// "A rising tide lifts all boats." - JKF, 1963

(function() {
    try {
        app.beginUndoGroup("Split Layer");
        var comp = app.project.activeItem;
        if (comp.selectedLayers.length > 1) {
            throw "Select a single layer";
        }
        var layer = comp.selectedLayers[0];
        app.executeCommand(2158);
        layer = comp.selectedLayers[0];
        layer.trackMatteType = TrackMatteType.NO_TRACK_MATTE;
    } catch(err) {
        alert(err);
    } finally {
        app.endUndoGroup();
    }
})();
